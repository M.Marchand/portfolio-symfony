<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PortFolioController extends AbstractController
{
    /**
     * @Route("/", name="port_folio")
     */
    public function index()
    {
        return $this->render('port_folio/index.html.twig', [
            'controller_name' => 'PortFolioController',
        ]);
    }
    /**
     * @Route("/presentation", name="presentation")
     */
    public function presentation()
    {
        return $this->render('port_folio/presentation.html.twig', [
            'controller_name' => 'PortFolioController',
        ]);
    }
    /**
     * @Route("/competences", name="competences")
     */
    public function competences()
    {
        return $this->render('port_folio/competences.html.twig', [
            'controller_name' => 'PortFolioController',
        ]);
    }
    /**
     * @Route("/projets", name="projets")
     */
    public function projets()
    {
        return $this->render('port_folio/projets.html.twig', [
            'controller_name' => 'PortFolioController',
        ]);
    }
    /**
     * @Route("/contact", name="contact")
     */
    public function contact()
    {
        $form= $this->createForm(ContactType::class);

        return $this->render('port_folio/contact.html.twig', [
            'controller_name' => 'PortFolioController',
            'form'=>$form->createView(),
        ]);
    }
}
